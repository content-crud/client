# contents-crud

- client สามารถ crud ได้ แต่ไม่สามารถ save รูปและไม่สามารถ save text editor ได้


## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Lints and fixes files
```
npm run lint
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).