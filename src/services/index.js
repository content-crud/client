import axios from 'axios'
import { BASEURL } from '/src/config/index'

export default () => {
    return axios.create({ baseURL: BASEURL });
}
