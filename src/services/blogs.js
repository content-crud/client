import HTTP from "./index";

export default {
  getBlogs() {
    return HTTP().get(`list/`);
  },
  getBlogId(data) {
    return HTTP().get(`list/${data}`);
  },
  insertBlog(data) {
    let res = HTTP().post("create/", data);
    return res;
  },
  updateBlog(data) {
    let res = HTTP().post(`update/${data.id}`, data);
    return res;
  },
  deleteBlog(data) {
    let res = HTTP().post(`delete/${data}`);
    return res;
  },
};
