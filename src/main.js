import Vue from "vue";
import Vuex from "vuex";
import VueRouter from "vue-router";
import {routes} from "./router/router";
import App from "./App.vue";
import StoreData from "./store/index";
import vuetify from "./plugins/vuetify";
import 'material-design-icons-iconfont/dist/material-design-icons.css';
// import plugin
import { TiptapVuetifyPlugin } from 'tiptap-vuetify';
// don't forget to import CSS styles
import 'tiptap-vuetify/dist/main.css';
// Vuetify's CSS styles 
import 'vuetify/dist/vuetify.min.css';

Vue.use(Vuex);
Vue.use(VueRouter);
Vue.config.productionTip = false;
Vue.use(TiptapVuetifyPlugin, {
  // the next line is important! You need to provide the Vuetify Object to this place.
  vuetify, // same as "vuetify: vuetify"
  // optional, default to 'md' (default vuetify icons before v2.0.0)
  iconsGroup: 'md'
});
const store = new Vuex.Store(StoreData);
const router = new VueRouter({
  mode: "history",
  base: process.env.BASE_URL,
  routes,
});


new Vue({
  router,
  store,
  vuetify,
  render: (h) => h(App),
}).$mount("#app");
