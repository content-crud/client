import ContentsPage from "../views/contents/main/content-page.vue";
import ContentAdd from "../views/contents/main/content-add.vue";
import ContentEdit from "../views/contents/main/content-edit.vue";
import ContentPreview from "../views/contents/main/content-preview.vue";

export const routes = [
  {
    path: "/",
    name: "MainPage",
    component: ContentsPage,
  },
  {
    path: "/content-add",
    name: "MainAdd",
    component: ContentAdd,
  },
  {
    path: "/content-edit",
    name: "MainEdit",
    component: ContentEdit,
  },
  {
    path: "/content-preview",
    name: "MainPreview",
    component: ContentPreview,
  },
];
