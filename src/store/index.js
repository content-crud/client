import BlogService from "../services/blogs";

export default({
  state: {
    listBlogs: null,
    listBlogId: null,
    isInsertBlog: false,
    isUpdateBlog: false,
    isDeleteBlog: false,
  },
  mutations: {
    SET_BLOGS(state, payload) {
      state.listBlogs = payload;
    },
    SET_BLOG_ID(state, payload) {
      state.listBlogId = payload;
    },
    SET_INSERT_BLOG(state, payload) {
      state.isInsertBlog = payload;
    },
    SET_UPDATE_BLOG(state, payload) {
      state.isUpdateBlog = payload;
    },
    SET_DELETE_BLOG(state, payload) {
      state.isDeleteBlog = payload;
    },
  },
  getters: {},
  actions: {
    async getBlogs({ commit }) {
      const result = await BlogService.getBlogs();
      commit("SET_BLOGS", result.data);
    },
    async getBlogId({ commit }, data) {
      const result = await BlogService.getBlogId(data);
      commit("SET_BLOG_ID", result.data);
    },
    async insertBlog({ commit }, data) {
      const result = await BlogService.insertBlog(data);
      if (result.data === "success") {
        commit("SET_INSERT_BLOG", result.data);
      } else {
        commit("SET_INSERT_BLOG", false);
      }
    },
    async updateBlog({ commit }, data) {
      const result = await BlogService.updateBlog(data);
      if (result.data === "success") {
        commit("SET_UPDATE_BLOG", result.data);
      } else {
        commit("SET_UPDATE_BLOG", false);
      }
    },
    async deleteBlog({ commit }, data) {
      const result = await BlogService.deleteBlog(data);
      if (result.data === "success") {
        commit("SET_DELETE_BLOG", result.data);
      } else {
        commit("SET_DELETE_BLOG", false);
      }
    },
  },   
  modules: {},
});
